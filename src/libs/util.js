import axios from 'axios';
import env from '../config/env';

let util = {

};
util.title = function(title) {
    title = title ? title + ' - Home' : 'iView project';
    window.document.title = title;
};

const server = 'http://activity.maxstudiozhuhai.com/api';
// const server = 'http://localhost:8081';


const ajaxUrl = env === 'development' ?
    server :
    env === 'production' ?
        server :
    'https://debug.url.com';

util.ajax = axios.create({
    baseURL: ajaxUrl,
    timeout: 30000
});

export default util;