const routers = [
    {
        path: '/login',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/login.vue'], resolve)
    },
    {
        path: '/entry/choose/:activityId',
        meta: {
            title: '选取入围'
        },
        component: (resolve) => require(['./views/chooseEntry.vue'], resolve)
    },
    {
        path: '/activity/manage',
        meta: {
            title: '活动管理'
        },
        component: (resolve) => require(['./views/activityManage/activityManage.vue'], resolve)
    },
    {
        path: '/activity/catalog',
        meta: {
            title: '分类管理'
        },
        component: (resolve) => require(['./views/catalogManage/catalogManage.vue'], resolve)
    },
    {
        path: '/vote/item/manage',
        meta: {
            title: '投票项目'
        },
        component: (resolve) => require(['./views/voteItemManage/voteItemManage.vue'], resolve)
    }
];
export default routers;